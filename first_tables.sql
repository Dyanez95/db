CREATE TABLE mascota(
mid CHAR(3) PRIMARY KEY,
nombre VARCHAR(10)
);

CREATE TABLE persona(
pid CHAR(3),
nombre VARCHAR(15),
PRIMARY KEY(pid)
);

ALTER TABLE mascota add (especie VARCHAR(10));

ALTER TABLE persona MODIFY nombre VARCHAR(20);

INSERT INTO mascota values(1,'Flash','Pez');