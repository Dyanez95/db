ALTER TABLE Driver ADD Gender CHAR(1) CONSTRAINT dgender_chk CHECK(Gender in ('F','M'));

UPDATE Driver SET Gender='M'
WHERE driverid='D344' OR driverid='D957'
;

UPDATE Driver SET Gender='F'
WHERE driverid='D456' OR driverid='D667'
;