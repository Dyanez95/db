CREATE TABLE driver(
	driverID CHAR(4) CONSTRAINT pk_driver PRIMARY KEY,
	dFName VARCHAR(10),
	Dlname VARCHAR(10),
	CONSTRAINT chck_id_d CHECK(driverID between 'D000' AND 'D999')
);

CREATE TABLE client(
	clientID CHAR(4) CONSTRAINT pk_client PRIMARY KEY,
	cFName VARCHAR(10),
	cLName VARCHAR(10),
	cAddress VARCHAR(20),
	CONSTRAINT chck_id_c CHECK(clientID between 'C000' AND 'C999')
);

CREATE TABLE job(
	jobID CHAR(4) CONSTRAINT pk_job PRIMARY KEY,
	pickupDate DATE,
	driverID CHAR(4) CONSTRAINT fk_driver REFERENCES driver(driverID),
	clientID CHAR(4) CONSTRAINT fk_client REFERENCES client(clientID),
	cost NUMBER CONSTRAINT chck_cost CHECK(cost>=100)
);

INSERT INTO driver (driverID, dFName, Dlname) VALUES ('D456','Jane','Smith');
INSERT INTO driver (driverID, dFName, Dlname) VALUES ('D667','Karen','Larson');
INSERT INTO driver (driverID, dFName, Dlname) VALUES ('D957','Steven','Jobs');
INSERT INTO driver (driverID, dFName, Dlname) VALUES ('D344','Tom','Seller');

INSERT INTO client (clientID,cFName,cLName,cAddress) VALUES ('C034','Ana','Aguilar','Patria 400, Guadalajara');
INSERT INTO client (clientID,cFName,cLName,cAddress) VALUES ('C089','Marco','Ruiz','Hidalgo 55, Guadalajara');
INSERT INTO client (clientID,cFName,cLName,cAddress) VALUES ('C019','Ana','Ibarra','Bolivar 345, Zapopan');
INSERT INTO client (clientID,cFName,cLName,cAddress) VALUES ('C039','Karen','Tellez','Petunias 76, Tonala');

INSERT INTO job (jobID,pickupDate,driverID,clientID,cost) VALUES ('1001',TO_DATE('25072006','DDMMYYYY'),'D456','C034',1000);
INSERT INTO job (jobID,pickupDate,driverID,clientID,cost) VALUES ('1102',TO_DATE('29072006','DDMMYYYY'),'D456','C034',700);
INSERT INTO job (jobID,pickupDate,driverID,clientID,cost) VALUES ('1203',TO_DATE('30072006','DDMMYYYY'),'D344','C034',300);
INSERT INTO job (jobID,pickupDate,driverID,clientID,cost) VALUES ('1334',TO_DATE('02082006','DDMMYYYY'),'D667','C089'550);
INSERT INTO job (jobID,pickupDate,driverID,clientID,cost) VALUES ('1455',TO_DATE('02082006','DDMMYYYY'),'D957','C019',450);
INSERT INTO job (jobID,pickupDate,driverID,clientID,cost) VALUES ('1676',TO_DATE('25082006','DDMMYYYY'),'D344','C019',800);