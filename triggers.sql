INSERT INTO RentalAgreement (rentalno,memberno,videono,dateout) VALUES(12,2,210087,SYSDATE);

UPDATE RentalAgreement SET datereturn=SYSDATE WHERE rentalno=12;

DELETE FROM RentalAgreement WHERE rentalno=12;


CREATE TABLE Librarian(
	essn char(9),
	name varchar(20),
	salary number(6),
	constraint pk_librarian PRIMARY KEY(essn)
);

CREATE TABLE Dependent(
	essn char(9),
	dependent_name varchar(15),
	sex char(1),
	bdate DATE,
	relationship varchar(8),
	constraint pk_dependent PRIMARY KEY(essn,dependent_name)
);

INSERT INTO Librarian values(987654321,'Juan Lopez',1000);

CREATE OR REPLACE TRIGGER dependiente
	BEFORE INSERT ON Dependent
	FOR EACH ROW
DECLARE
	x NUMBER;
BEGIN
	SELECT count(*) INTO x FROM Dependent d
	WHERE d.essn=:new.essn;
	IF x<3 THEN
		UPDATE Librarian
		SET salary=salary+100
		WHERE essn=:new.essn;
	ELSE
		raise_application_error(-20000,'Demasiados dependientes');
	END IF;
END;
/

INSERT INTO Dependent VALUES(987654321,'Juanita','F','04-21-14','Daughter');
INSERT INTO Dependent VALUES(987654321,'Lupita','F','04-21-14','Daughter');

