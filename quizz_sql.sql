#1
SELECT cfname, clname, viajes, TotalCosto
FROM Client,(
	SELECT c.clientid, COUNT(*) AS Viajes, SUM(cost) AS TotalCosto
	FROM Client c, Job j
	WHERE c.clientid=j.clientid
	GROUP BY c.clientid
)r1
WHERE Client.clientid=r1.clientid
;

#2
SELECT title, directorName
FROM Director, Video
WHERE Director.directorno=Video.directorno
;

#3
SELECT MemberVi.fname, MemberVi.lname
FROM MemberVi,(
	SELECT r1.MemberNo
	FROM (
		SELECT MemberNo
		FROM MemberVi
	)r1
	MINUS
	(
		SELECT DISTINCT MemberNo
		FROM RentalAgreement
	)
)r2
WHERE MemberVi.MemberNo=r2.MemberNo
;