
CREATE VIEW vista3 AS
SELECT title,category, COUNT(*) AS Copias
FROM Video NATURAL JOIN VideoForRent
GROUP BY category, title
HAVING COUNT(*)>1
;

ERROR at line 1:
ORA-01779: cannot modify a column which maps to a non key-preserved table

