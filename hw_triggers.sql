CREATE OR REPLACE TRIGGER validarRenta
	BEFORE INSERT OR UPDATE ON RentalAgreement
	FOR EACH ROW
DECLARE
	x NUMBER;
BEGIN

	SELECT available INTO x FROM VideoForRent vfr
	WHERE vfr.videono=:new.videono;

	IF :new.datereturn IS NOT NULL THEN
		UPDATE VideoForRent
		SET available=1
		WHERE videono=:new.videono;
	ELSIF x!=0 THEN
		UPDATE VideoForRent
		SET available=0
		WHERE videono=:new.videono;
	ELSE
		raise_application_error(-20000,'video not available');
	END IF;
END;
/