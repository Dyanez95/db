#1
SELECT sex, COUNT(*) AS cantidad
FROM MemberVi
GROUP BY sex
;

#2
SELECT COUNT(*) AS Cardinalidad
FROM Video
;

#3
SELECT fname, lname
FROM MemberVi
ORDER BY fname, lname
;

#4
SELECT title, DirectorName
FROM Video v, Director d
WHERE v.DirectorNo=d.DirectorNo
;

#5
SELECT SUM(price) AS TotalPrice
FROM(
	SELECT v.catalogno, price
	FROM Video v, VideoForRent vfr
	WHERE v.catalogno=vfr.catalogno
)
;


#6
SELECT title, COUNT(videoNo) AS Copias
FROM VideoForRent vr, Video v
WHERE vr.catalogno=v.catalogno
GROUP BY title
;

#7
SELECT category, COUNT(category) AS cantidadporcategoria
FROM Video
GROUP BY category
ORDER BY category
;

#8
SELECT title, cost
FROM(
	SELECT MAX(dailyrental) AS cost
	FROM Video
) r, Video v
WHERE v.dailyrental=r.cost
;

#9
SELECT v.title, SUM(dailyrental) AS rent
FROM (
	SELECT catalogno
	FROM VideoForRent vfr, RentalAgreement ra
	WHERE vfr.videoNo=ra.videoNo
) r, Video v
WHERE r.catalogno=v.catalogno
GROUP BY v.title
;

#10
SELECT DISTINCT fname, lname
FROM MemberVi m, RentalAgreement ra
WHERE m.memberno=ra.memberno AND (SYSDATE>datereturn OR datereturn IS NULL)
;