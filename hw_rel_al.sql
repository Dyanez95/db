#1
SELECT cfname, clname, caddress FROM client;

#2
SELECT * 
	FROM driver
	WHERE dlname='Smith' OR dlname='Seller'
;

#3
SELECT DISTINCT dfname, dlname, cfname, clname
	FROM Driver d, Client c, Job j
	WHERE j.driverid=d.driverid AND j.clientid=c.clientid
;

#4
SELECT DISTINCT dfname, dlname, cfname, clname
	FROM Driver, Client
;

#4
SELECT cfname AS Name, clname AS LName
FROM Client UNION
SELECT dfname, dlname
FROM Driver
;

#5
SELECT c.clientid, clname, cfname, caddress
FROM(
	SELECT clientid
	FROM Client
	MINUS
	SELECT clientid
	FROM Job
	) r, Client c
WHERE r.clientid=c.clientid
;

#6
SELECT DISTINCT cfname, clname, caddress
	FROM Client c, Job j
	WHERE c.clientid=j.clientid
;

#7
SELECT DISTINCT c.clientid
FROM Client c, Driver d, Job j
;

#8
SELECT COUNT(jobid) as TotalViajes, SUM(cost) as TotalCost
FROM Job
;

#9
SELECT d.driverid, dfname, dlname, r.totalC
FROM(
	SELECT driverid, SUM(cost) as totalC
	FROM Job
	GROUP BY driverid
) r, Driver d
WHERE r.driverid=d.driverid
;

#9

SELECT d.driverid, dfname, SUM(cost) as TotalCost
FROM Driver d, Job j
WHERE d.driverid=j.driverid
GROUP BY d.driverid, dfname
#HAVING SUM(cost)>2000 AND count(*)>2
;

#10
SELECT c.clientid, cfname, clname, caddress, r.totalC
FROM Client c LEFT JOIN(
	SELECT clientid, SUM(cost) as totalC
	FROM Job
	GROUP BY clientid
) r ON c.clientid=r.clientid
;