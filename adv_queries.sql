#1
SELECT title
FROM Video,
(
	SELECT catalogNo, rentals
	FROM(
		SELECT videoNo, rentals
		FROM(
			SELECT videoNo, COUNT(*) AS rentals
			FROM RentalAgreement
			GROUP BY videoNo
		)
		HAVING rentals=(
			SELECT MAX(cRentals)
			FROM(
				SELECT videoNo,COUNT(*) as cRentals
				FROM RentalAgreement
				GROUP BY videoNo
			)
		)
		GROUP BY videoNo, rentals
	) r1, VideoForRent vfr
	WHERE r1.videoNo=vfr.videoNo
) r2
WHERE Video.catalogNo=r2.catalogNo
;

#2
SELECT fName, lName
FROM MemberVi
WHERE MemberVi.DOB=SYSDATE
;

#3
SELECT title
FROM Video,
(
	SELECT catalogNo
	FROM Video
	MINUS
	(
		SELECT DISTINCT Video.catalogNo
		FROM Video,
		(
			SELECT VideoForRent.videoNo, catalogNo
			FROM VideoForRent, RentalAgreement
			WHERE VideoForRent.videoNo=RentalAgreement.videoNo
		) r1
		WHERE Video.catalogNo=r1.catalogNo
	)
) r2
WHERE Video.catalogNo=r2.catalogNo
;

#4
SELECT MemberVi.fName, MemberVi.lName
FROM MemberVi,
(
	SELECT memberno,EXTRACT(YEAR FROM DOB) AS YOB
	FROM MemberVi
	WHERE MemberVi.fName='Lorna' AND MemberVi.lName='Smith'
) r1
WHERE EXTRACT(YEAR FROM MemberVi.DOB)=YOB AND MemberVi.memberNo!=r1.memberNo
;


#5
SELECT fname, lname, rentals AS posicion
FROM MemberVi,(
	SELECT memberNo,COUNT(*) AS rentals
	FROM RentalAgreement
	GROUP BY memberno
) r1
WHERE MemberVi.memberno=r1.memberno
GROUP BY fName,lName,rentals
HAVING rentals=(
	SELECT MAX(cRentals1)
	FROM(
		SELECT memberno, COUNT(*) as cRentals1
		FROM RentalAgreement
		GROUP BY memberno
	)
) OR rentals=(
	SELECT MIN(cRentals2)
	FROM(
		SELECT memberno, COUNT(*) as cRentals2
		FROM RentalAgreement
		GROUP BY memberno
	)
)
ORDER BY rentals DESC
;

#6
SELECT DISTINCT fName, lName
FROM MemberVi mv,
(
	SELECT RentalAgreement.memberno, catalogNo
	FROM VideoForRent, RentalAgreement
	WHERE VideoForRent.videoNo=RentalAgreement.videoNo
) r1, (
	SELECT DISTINCT catalogNo
	FROM (
		SELECT RentalAgreement.memberno, catalogNo
		FROM VideoForRent, RentalAgreement
		WHERE VideoForRent.videoNo=RentalAgreement.videoNo
		) tmp, MemberVi
	WHERE MemberVi.memberno=tmp.memberno AND MemberVi.fname='Gillian' AND MemberVi.lname='Peters'
) r2
WHERE mv.memberno=r1.memberno AND NOT(mv.fname='Gillian' AND mv.lname='Peters') AND r1.catalogNo=r2.catalogNo
;