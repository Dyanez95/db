SELECT MemberVi.fname, MemberVi.lname, rentals
FROM MemberVi, (
	SELECT memberno, COUNT(*) AS rentals
	FROM RentalAgreement
	GROUP BY memberno
) r
WHERE MemberVi.memberno=r.memberno
;