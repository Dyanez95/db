SELECT DISTINCT clientid, cfname, clname, caddress
FROM Client NATURAL JOIN job;

SELECT *
FROM Client
WHERE clientid IN (SELECT clientid FROM job)
;

SELECT *
FROM Client c
WHERE NOT EXISTS (
	SELECT *
	FROM job j
	WHERE j.clientid=c.clientid
)
;

SELECT clientid, cfname, clname, caddress, total
FROM Client NATURAL JOIN (
	SELECT clientid, SUM(cost) AS TOTAL
	FROM job
	GROUP BY clientid
)
;



#1
SELECT cfname, clname, jobid, pickupdate
FROM Client c LEFT JOIN Job j ON c.clientid=j.clientid
;


#2
SELECT dfname, dlname, total
FROM Driver d NATURAL JOIN(
	SELECT driverid, SUM(cost) AS total
	FROM job
	GROUP BY driverid
)
;

#3
SELECT dfname, dlname
FROM Driver d, Job j, Client c
WHERE d.driverid=j.driverid 
	AND c.clientid=j.clientid
	AND c.cfname='Ana' AND (c.clname='Ibarra' OR c.clname='Aguilar')
GROUP BY dfname, dlname
HAVING COUNT(DISTINCT c.clientid)=(
	SELECT COUNT(*)
	FROM Client
	WHERE Client.cfname='Ana' AND (Client.clname='Ibarra' OR Client.clname='Aguilar')
)
;
