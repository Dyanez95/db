SELECT r1.cfname, r1.clname
FROM (
	SELECT Client.clientid, Driver.driverid, cfname, clname
	FROM Client, Driver, Job
	WHERE Client.clientid=Job.clientid AND Driver.driverid=Job.driverid
) r1
GROUP BY cfname, clname
HAVING COUNT(DISTINCT r1.driverid)=(
	SELECT COUNT(*)
	FROM Driver
)
;

SELECT r1.cfname, r1.clname
FROM (
	SELECT Client.clientid, Driver.driverid, cfname, clname
	FROM Client, Driver, Job
	WHERE Client.clientid=Job.clientid AND Driver.driverid=Job.driverid AND Driver.gender='M'
) r1
GROUP BY cfname, clname
HAVING COUNT(DISTINCT r1.driverid)=(
	SELECT COUNT(*)
	FROM Driver
	WHERE Driver.gender='M'
)
;