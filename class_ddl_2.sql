-- ITESM Campus Guadalajara
-- Bases de Datos
-- Mat. Ana Delia Esparza Soto
-- Base de Datos Stayhome

-- Diego Yanez Llamas A01228648

CREATE TABLE Director
(DirectorNo       CHAR(5) CONSTRAINT director_pk PRIMARY KEY,
 DirectorName     VARCHAR(25) 
)
;

CREATE TABLE Video
(catalogNo          INT CONSTRAINT video_pk NOT NULL PRIMARY KEY,
 title              VARCHAR(30),
 certificate        VARCHAR(10),
 category           VARCHAR(10),
 dailyRental        DEC (3,2),
 price              DEC (5,2),
 DirectorNo         CHAR(5) CONSTRAINT video_dir_fk REFERENCES Director(DirectorNo)
)
;

CREATE TABLE VideoForRent
(videoNo            INT NOT NULL,
available           INT CHECK (available IN (0,1)),
 catalogNo          INT,
 CONSTRAINT vfr_pk PRIMARY KEY(videoNo),
 CONSTRAINT vfr_video_fk FOREIGN KEY(catalogNo) REFERENCES Video(catalogNo)
)
;

CREATE TABLE Member
(memberNo        INT CONSTRAINT member_pk PRIMARY KEY,
 fName           VARCHAR(15),
 lName           VARCHAR(15),
 sex             CHAR(1) DEFAULT 'M' CONSTRAINT sex_chk CHECK(sex IN('M','F')),
 DOB             DAte,
 address         VARCHAR(50),
 dateJoined      DATE
)
;

CREATE TABLE RentalAgreement
(rentalNo        INT,
 memberNo        INT,
 videoNo         INT,
 dateOut         DATE DEFAULT SYSDATE,
 dateReturn      DATE
)
;

ALTER SESSION SET NLS_DATE_FORMAT='MM-DD-YYYY';

ALTER TABLE Director ADD CONSTRAINT  directorRule CHECK(DirectorNo BETWEEN 'D0000' AND 'D9999');

ALTER TABLE RentalAgreement ADD CONSTRAINT rental_pk PRIMARY KEY(rentalNo);
ALTER TABLE RentalAgreement ADD CONSTRAINT ra_vfr_fk FOREIGN KEY(videoNo) REFERENCES VideoForRent(videoNo);
ALTER TABLE RentalAgreement ADD CONSTRAINT ra_mem_fk FOREIGN KEY(memberNo) REFERENCES Member(memberNo);
ALTER TABLE RentalAgreement ADD CONSTRAINT date_chck CHECK(dateReturn>dateOut);

INSERT INTO Director VALUES ('D0078', 'Stephen Herek')
;
INSERT INTO Director VALUES ('D1001', 'Roger Spottiswoode')
;
INSERT INTO Director VALUES ('D3765', 'Jane Emmerick')
;
INSERT INTO Director VALUES ('D4576', 'John Woo')
;
INSERT INTO Director VALUES ('D5743', 'Michael Bay')
;
INSERT INTO Director VALUES ('D7834', 'Sally Nichols')
;
INSERT INTO Video VALUES (207132, 'Tomorrow Never Dies','12','Action',5,21.99,'D1001')
;
INSERT INTO Video VALUES (330553, 'Face/Off','12','Thriller',5,31.99,'D4576')
;
INSERT INTO Video VALUES (445624, 'The Rock','18','Action',4,29.99,'D5743')
;
INSERT INTO Video VALUES (634817, 'Independence Day','PG','Sci-Fi',4.5,32.99,'D3765')
;
INSERT INTO Video VALUES (781132, '101 Dalmatians','U','Children',4,18.5,'D0078')
;
INSERT INTO Video VALUES (902355, 'Primary Colors','U','Comedy',4.5,14.5,'D7834')
;
INSERT INTO VideoForRent VALUES (178643, 0,634817)
;
INSERT INTO VideoForRent VALUES (199004, 1,207132)
;
INSERT INTO VideoForRent VALUES (200900, 1,330553)
;
INSERT INTO VideoForRent VALUES (210087, 1,902355)
;
INSERT INTO VideoForRent VALUES (243431, 1,634817)
;
INSERT INTO VideoForRent VALUES (245456, 1,207132)
;
INSERT INTO VideoForRent VALUES (245457, 1,207132)
;
INSERT INTO VideoForRent VALUES (317411, 1,781132)
;
INSERT INTO Member VALUES (1,'Karen', 'Parker','F','2-22-1972','22 Greenway Drive, Glas;w, G12 8DS','1-6-1991')
;
INSERT INTO Member VALUES (2,'Gillian', 'Peters','F','3-9-1954','89 Redmond Road, Glas;w, G11 9YR','4-19-1995')
;
INSERT INTO Member VALUES (3,'Bob', 'Adams','M','11-15-1974','57 Littleway Road, Glas;w, G3 6DS','1-6-1998')
;
INSERT INTO Member VALUES (4,'Don', 'Nelson','M','12-12-1951','123 Suffolk Lane,Glas;w, G15 1RC','4-7-2000')
;
INSERT INTO Member VALUES (5,'Lorna', 'Smith','F','1-1-1972','Flat 5A London Road, Glas;w, G3','1-11-1998')
;
INSERT INTO Member VALUES (6,'Dan', 'White','M','5-5-1960','200 Great Western Road, Glas;w, G11 9JJ','5-5-2001')
;
INSERT INTO RentalAgreement (rentalNo,memberNo, videoNo, dateOut, dateReturn) VALUES (1,1, 178643,'2-5-2000','2-7-2000')
;
INSERT INTO RentalAgreement (rentalNo,memberNo, videoNo, dateOut, dateReturn) VALUES (2,1, 199004,'2-5-2000','2-7-2000')
;
INSERT INTO RentalAgreement (rentalNo,memberNo, videoNo, dateOut, dateReturn) VALUES (3,1, 245456,'2-4-2000','2-6-2000')
;
INSERT INTO RentalAgreement (rentalNo,memberNo, videoNo, dateOut, dateReturn) VALUES (4,1, 243431,'2-4-2000','2-6-2000')
;
INSERT INTO RentalAgreement (rentalNo,memberNo, videoNo, dateOut, dateReturn) VALUES (5,3, 199004,'11-11-1999','11-12-1999')
;
INSERT INTO RentalAgreement (rentalNo,memberNo, videoNo, dateOut, dateReturn) VALUES (6,2, 245456,'11-11-1999','11-13-1999')
;
INSERT INTO RentalAgreement (rentalNo,memberNo, videoNo, dateOut, dateReturn) VALUES (7,4, 178643,'11-14-1999','11-16-1999')
;
INSERT INTO RentalAgreement (rentalNo,memberNo, videoNo, dateOut, dateReturn) VALUES (8,3, 243431,'11-11-1999','11-13-1999')
;
INSERT INTO RentalAgreement (rentalNo,memberNo, videoNo, dateOut, dateReturn) VALUES (9,1, 245457,'11-12-1999','11-14-1999')
;
INSERT INTO RentalAgreement (rentalNo,memberNo, videoNo, dateOut, dateReturn) VALUES (10,3, 199004,'8-10-2000',NULL)
;
INSERT INTO RentalAgreement (rentalNo,memberNo, videoNo, dateOut, dateReturn) VALUES (11,2, 199004,'8-17-2000','8-20-2000')
;
ALTER TABLE Member ADD COLUMN actividad VARCHAR(20);